# editorconfig-vim for Pearl

[EditorConfig](http://editorconfig.org/) plugin for Vim

## Details

- Plugin: https://github.com/editorconfig/editorconfig-vim
- Pearl: https://github.com/pearl-core/pearl
